//
//  CountryModel.swift
//  ViewController. Single Responsibility Principle.
//
//  Created by Jhonny Green on 31.03.2020.
//  Copyright © 2020 Алексей Пархоменко. All rights reserved.
//

import Foundation

struct Country: Decodable {
    var Id: String
    var Time: String
    var Name: String
    var Image: String?
}
